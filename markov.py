#!/usr/bin/python
import copy
import sys
#returns N,M,[p1,p2,p3],r,y, list of fault states, l of terminal states, l of special states
def loadWorld(fn):
	fh=open(fn,'r')
	lines=fh.read().splitlines()
	N=int(lines[0])
	M=int(lines[1])
	P=[float(lines[2]),float(lines[3]),float(lines[4])]
	r=float(lines[5])
	y=float(lines[6])
	coord=lines[7].split(',', 2 )
	StartingPoint=[int(coord[0]),int(coord[1])]
	iterator=8
	Fault=[]
	Term=[]
	Spec=[]
	while iterator<len(lines):
		if lines[iterator]=='f':
			iterator+=1
			while iterator<len(lines) and lines[iterator]!='t' and lines[iterator]!='b' :
				coord=lines[iterator].split(',', 2 )
				Fault.append([int(coord[0]),int(coord[1])])
				iterator+=1
		if iterator<len(lines) and lines[iterator]=='t' :
			iterator+=1
			while iterator<len(lines) and lines[iterator]!='f' and lines[iterator]!='b' :
				coord=lines[iterator].split(',', 3 )
				Term.append([int(coord[0]),int(coord[1]),float(coord[2])])
				iterator+=1
		if iterator<len(lines) and lines[iterator]=='b':
			iterator+=1
			while iterator<len(lines) and lines[iterator]!='t' and lines[iterator]!='f' :
				coord=lines[iterator].split(',', 3 )
				Spec.append([int(coord[0]),int(coord[1]),float(coord[2])])
				iterator+=1
		
	return N,M,P,r,y,Fault,Term,Spec,StartingPoint


def MakeRewardsMatrix(r,Fault,Term,Spec):
	Rewards = [[r for x in range(M)] for x in range(N)]
	for x in Fault:
		Rewards[x[0]-1][x[1]-1]='f'
	for x in Spec:
		Rewards[x[0]-1][x[1]-1]=x[2]
	for x in Term:
		Rewards[x[0]-1][x[1]-1]=x[2]
	return Rewards
	
def MakeUtilitiesMatrix(r,Fault,Term,Spec):
	Rewards = [[r for x in range(M)] for x in range(N)]
	for x in Fault:
		Rewards[x[0]-1][x[1]-1]='f'
	for x in Spec:
		Rewards[x[0]-1][x[1]-1]=x[2]
	for x in Term:
		Rewards[x[0]-1][x[1]-1]=x[2]
	return Rewards


#returns new utilities and max difference between old and new
def UpdateUtilities(Utilities,Rewards,N,M,y,P,Spec):
	NewUtilities = copy.deepcopy(Utilities)
	for i in range(N):
		for j in range(M):
			if Utilities[i][j]!='f':
				if i<N-1:
					RightUtil=Utilities[i+1][j]
				else:
					RightUtil=Utilities[N-1][j]
				if i==0:
					LeftUtil=Utilities[0][j]
				else:
					LeftUtil=Utilities[i-1][j]
				if j<M-1:
					UpUtil=Utilities[i][j+1]
				else:
					UpUtil=Utilities[i][M-1]
				if j==0:
					DownUtil=Utilities[i][0]
				else:
					DownUtil=Utilities[i][j-1]
				
				if RightUtil == 'f':
					RightUtil=Utilities[i][j]
				if UpUtil == 'f':
					UpUtil=Utilities[i][j]
				if LeftUtil == 'f':
					LeftUtil=Utilities[i][j]
				if DownUtil == 'f':
					DownUtil=Utilities[i][j]
				Up=UpUtil*P[0]+LeftUtil*P[1]+RightUtil*P[2]+DownUtil*(1-P[0]-P[1]-P[2])
				Right=RightUtil*P[0]+UpUtil*P[1]+DownUtil*P[2]+LeftUtil*(1-P[0]-P[1]-P[2])
				Left=LeftUtil*P[0]+DownUtil*P[1]+UpUtil*P[2]+RightUtil*(1-P[0]-P[1]-P[2])
				Down=DownUtil*P[0]+RightUtil*P[1]+LeftUtil*P[2]+UpUtil*(1-P[0]-P[1]-P[2])
				NewUtilities[i][j]=Rewards[i][j]+y*max(Up,Down,Right,Left)
				
				for x in Term:
					NewUtilities[x[0]-1][x[1]-1]=x[2]
					
	
		
	diff=0		
	for i in range(N):
		for j in range(M):
			if Utilities[i][j]!='f':
				if abs(Utilities[i][j]-NewUtilities[i][j])>diff:
					diff=abs(Utilities[i][j]-NewUtilities[i][j])
					
	return diff, NewUtilities
	
	
def DisplaySolution(Utilities,N,M):
	for j in range(M):
		string=''
		for i in range(N):
			if Utilities[i][M-1-j] !='f':
				string+=str(round(Utilities[i][M-1-j],4))+'\t'
			else:
				string+='X\t'
		print string


def WriteSolution(Utilities,N,M,name):
	fh=open(name, 'w+')
	for j in range(M):
		string=''
		for i in range(N):
			if Utilities[i][M-1-j] !='f':
				string+=str(round(Utilities[i][M-1-j],4))+'\t'
			else:
				string+='X\t'
		fh.write( string+'\n')

def DisplayPolicy(Utilities,N,M,P,Spec):
	Policy = [['x\t' for x in range(M)] for x in range(N)]
	for j in range(M):
		for i in range(N):
			if Utilities[i][j] !='f':
				if i<N-1:
					right=Utilities[i+1][j]
				else:
					right=Utilities[i][j]
				if i>0:
					left=Utilities[i-1][j]
				else:
					left=Utilities[i][j]
				if j<M-1:
					up=Utilities[i][j+1]
				else:
					up=Utilities[i][j]
				if j>0:
					down=Utilities[i][j-1]
				else:
					down=Utilities[i][j]
				if up=='f':
					up=Utilities[i][j]
				if down=='f':
					down=Utilities[i][j]
				if left=='f':
					left=Utilities[i][j]
				if right=='f':
					right=Utilities[i][j]
				
				Up=up*P[0]+left*P[1]+right*P[2]+down*(1-P[0]-P[1]-P[2])
				Right=right*P[0]+up*P[1]+down*P[2]+left*(1-P[0]-P[1]-P[2])
				Left=left*P[0]+down*P[1]+up*P[2]+right*(1-P[0]-P[1]-P[2])
				Down=down*P[0]+right*P[1]+left*P[2]+up*(1-P[0]-P[1]-P[2])
				maxim=max(Up,Down,Left,Right)
				if maxim==Up:
					Policy[i][j]='^\t'
				else:
					if maxim==Down:
						Policy[i][j]='v\t'
					else:
						if maxim==Left:
							Policy[i][j]='<\t'
						else:
							Policy[i][j]='>\t'
			
	for x in Term:
		Policy[x[0]-1][x[1]-1]='T'
	for j in range(M):
		string=''
		for i in range(N):
			string+=str(Policy[i][M-1-j])
		print string

def WritePlot(Utilities,fh,no):
	fh.write(str(no)+' ')
	for j in range(M):
		for i in range(N):
			if Utilities[i][j] !='f':
				fh.write(str(Utilities[i][j])+' ')	
	fh.write( '\n')
		


####################   Main  ###################################################
fh=str(sys.argv[1])
N,M,P,r,y,Fault,Term,Spec,StartingPoint=loadWorld(fh)



Rewards=MakeRewardsMatrix(r,Fault,Term,Spec)
Utilities=MakeUtilitiesMatrix(r,Fault,Term,Spec)

fh_gnu=open('plot.txt', 'w+')
diff=1.0
no=0
WritePlot(Utilities,fh_gnu,no)
while diff>0.0001:
	no+=1
	diff,Utilities=UpdateUtilities(Utilities,Rewards,N,M,y,P,Spec)
	WritePlot(Utilities,fh_gnu,no)

DisplaySolution(Utilities,N,M)
DisplayPolicy(Utilities,N,M,P,Spec)


